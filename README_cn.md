# C++ WebSSH

[English](./README.md) | 简体中文

本项目是一个C++的WebSSH项目，基于websocketpp + unzip + libssh2 + libgcrypt库，整体项目从骨架项目https://github.com/eaglexmw-gmail/WebSocketSrvSkel.git改造而来。
其中Web部分来源于https://github.com/mei-rune/web-terminal的static目录，仅做少量修改。

## 1.目录分级

前后台的目录分开管理，其中前台代码集中在frontend目录下，分子项目管理；后台服务代码在backend目录下管理。

### 1.1 frontend目录

每个前台子项目目录下，要求必须有一个dist目录，此目录内容存放的是，会被打包进服务程序运行文件的前台最终代码（会被zip压缩）

### 1.2 backend目录

后台目录下，分3rd、bin、inc、lib、src等目录

#### 1.2.1 3rd目录

此目录存放后台服务依赖的第三方库，有websocketpp、asio、cmdline及unzip等。

#### 1.2.2 bin目录

此目录存放编译出来的服务程序

#### 1.2.3 inc目录

此目录存放服务程序公共依赖的头文件，其中主要是前台程序打包出来的web_assets_xxx.h文件。

#### 1.2.4 lib目录

此目录存放编译出来的库文件或第三方库的库文件

#### 1.2.5 src目录

后台服务的主目录，各个子项目分目录存放，其中common目录存放的是各个子项目共享的一些库代码，会被编译成库文件，其余目录应该编译成服务程序运行文件。

## 1.3 编译

根目录下运行
```
make
```
会生成相应服务程序。

如需要调试，应运行
```
make BUILD_TYPE=Debug
```
生成带调试信息的服务程序

## 1.4 armv7交叉编译

根目录下运行
```
make CROSS=arm-himix200-linux- CPU_TYPE=armv7l CROSS_CFLAGS=" -mcpu=cortex-a7 -mfloat-abi=hard -mfpu=neon-vfpv4 " CROSS_CXXFLAGS=" -mcpu=cortex-a7 -mfloat-abi=hard -mfpu=neon-vfpv4 " CROSS_LDFLAGS=" -mcpu=cortex-a7 -mfloat-abi=hard -mfpu=neon-vfpv4 " 
```
会生成相应服务程序。

如需要调试，应运行
```
make CROSS=arm-himix200-linux- CPU_TYPE=armv7l BUILD_TYPE=Debug CROSS_CFLAGS=" -mcpu=cortex-a7 -mfloat-abi=hard -mfpu=neon-vfpv4 " CROSS_CXXFLAGS=" -mcpu=cortex-a7 -mfloat-abi=hard -mfpu=neon-vfpv4 " CROSS_LDFLAGS=" -mcpu=cortex-a7 -mfloat-abi=hard -mfpu=neon-vfpv4 " 
```
生成带调试信息的服务程序

## 1.5 aarch64交叉编译

根目录下运行
```
make CROSS=aarch64-himix100-linux- CPU_TYPE=aarch64
```
会生成相应服务程序。

如需要调试，应运行
```
make CROSS=aarch64-himix100-linux- CPU_TYPE=aarch64 BUILD_TYPE=Debug
```
生成带调试信息的服务程序

## 1.6 当前项目

当前项目中，项目编译链接完成，运行后，根据传入参数监听相应端口，访问http://127.0.0.1:xxxx/即可查看服务内容。

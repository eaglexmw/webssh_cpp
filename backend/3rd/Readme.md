# x86_64 Linux下库的编译

## libgpg-error
    
```
git checkout libgpg-error-1.46
./autogen.sh
CFLAGS="-fPIC" ./configure --prefix=/usr --enable-static --disable-shared --disable-doc --disable-tests --disable-languages --disable-rpath --enable-install-gpg-error-config
make
make DESTDIR=${PWD} install
sed -i "s|prefix=/usr|prefix=${PWD}/usr|g" usr/bin/gpg-error-config
```

## libgcrypt

```
git checkout libgcrypt-1.10.1
./autogen.sh
CFLAGS="-I${PWD}/../libgpg-error/usr/include" LDFLAGS="-L${PWD}/../libgpg-error/usr/lib" ./configure --prefix=/usr --enable-static --disable-shared --disable-doc --with-pic --with-libgpg-error-prefix=${PWD}/../libgpg-error/usr
make
make DESTDIR=${PWD} install
sed -i "s|=\"/usr\"|=\"${PWD}/usr\"|g" usr/bin/libgcrypt-config
sed -i "s|/usr/lib/libgpg-error.la|${PWD}/../libgpg-error/usr/lib/libgpg-error.la|g" usr/lib/libgcrypt.la
```

## libssh2

```
git checkout libssh2-1.10.0
sed -i "s|SUBDIRS = src tests docs|SUBDIRS = src|g" Makefile.am
autoscan
autoconf
libtoolize --automake --copy --debug --force
automake --add-missing
CFLAGS="-g -O2 -I${PWD}/../libgpg-error/usr/include -I${PWD}/../libgcrypt/usr/include" LDFLAGS="-L${PWD}/../libgpg-error/usr/lib -L${PWD}/../libgcrypt/usr/lib" ./configure --prefix=/usr --disable-shared --disable-rpath --disable-examples-build --with-pic --with-crypto=libgcrypt --with-libgcrypt-prefix=${PWD}/../libgcrypt/usr --without-libz
make
make DESTDIR=${PWD} install
```

# armv7l Linux下库的交叉编译

编译器：arm-himix200-linux，来自海思Hi3516dv300SDK，编译出的obj/elf文件，格式应为ubuntu-armhf的格式（private flags = 5000400: [Version5 EABI] **[hard-float ABI]**）

## libgpg-error

需要先编译一下Host版本，然后再执行交叉编译

```
git checkout libgpg-error-1.46
./autogen.sh
CFLAGS="-fPIC -mcpu=cortex-a7 -mfloat-abi=hard -mfpu=neon-vfpv4 " ./configure --prefix=/usr --host=arm-himix200-linux --enable-static --disable-shared --disable-doc --disable-tests --disable-languages --disable-rpath --enable-install-gpg-error-config
make
make DESTDIR=${PWD} install
sed -i "s|prefix=/usr|prefix=${PWD}/usr|g" usr/bin/gpg-error-config
```

## libgcrypt

```
git checkout libgcrypt-1.10.1
./autogen.sh
CFLAGS="-I${PWD}/../libgpg-error/usr/include -mcpu=cortex-a7 -mfloat-abi=hard -mfpu=neon-vfpv4 " LDFLAGS="-L${PWD}/../libgpg-error/usr/lib" ./configure --prefix=/usr  --host=arm-himix200-linux --enable-static --disable-shared --disable-doc --with-pic --with-libgpg-error-prefix=${PWD}/../libgpg-error/usr
make
make DESTDIR=${PWD} install
sed -i "s|=\"/usr\"|=\"${PWD}/usr\"|g" usr/bin/libgcrypt-config
sed -i "s|/usr/lib/libgpg-error.la|${PWD}/../libgpg-error/usr/lib/libgpg-error.la|g" usr/lib/libgcrypt.la
```

## libssh2

```
git checkout libssh2-1.10.0
sed -i "s|SUBDIRS = src tests docs|SUBDIRS = src|g" Makefile.am
autoscan
autoconf
libtoolize --automake --copy --debug --force
automake --add-missing
CFLAGS="-g -O2 -I${PWD}/../libgpg-error/usr/include -I${PWD}/../libgcrypt/usr/include -mcpu=cortex-a7 -mfloat-abi=hard -mfpu=neon-vfpv4 " LDFLAGS="-L${PWD}/../libgpg-error/usr/lib -L${PWD}/../libgcrypt/usr/lib -mcpu=cortex-a7 -mfloat-abi=hard -mfpu=neon-vfpv4 " ./configure --prefix=/usr --host=arm-himix200-linux --disable-shared --disable-rpath --disable-examples-build --with-pic --with-crypto=libgcrypt --with-libgcrypt-prefix=${PWD}/../libgcrypt/usr --without-libz
make
make DESTDIR=${PWD} install
```

# aarch64 Linux下库的交叉编译

编译器：aarch64-himix100-linux，来自海思Hi3559 SDK（注意，需要执行一下，export LC_ALL=C, export LANGUAGE="C", export LANG="C"，否则编译时gcc会crash）

## libgpg-error

需要先编译一下Host版本，然后再执行交叉编译

```
git checkout libgpg-error-1.46
./autogen.sh
CFLAGS="-fPIC " ./configure --prefix=/usr --host=aarch64-himix100-linux --enable-static --disable-shared --disable-doc --disable-tests --disable-languages --disable-rpath --enable-install-gpg-error-config
make
make DESTDIR=${PWD} install
sed -i "s|prefix=/usr|prefix=${PWD}/usr|g" usr/bin/gpg-error-config
```

## libgcrypt

```
git checkout libgcrypt-1.10.1
./autogen.sh
CFLAGS="-I${PWD}/../libgpg-error/usr/include " LDFLAGS="-L${PWD}/../libgpg-error/usr/lib" ./configure --prefix=/usr  --host=aarch64-himix100-linux --enable-static --disable-shared --disable-doc --with-pic --with-libgpg-error-prefix=${PWD}/../libgpg-error/usr
make
make DESTDIR=${PWD} install
sed -i "s|=\"/usr\"|=\"${PWD}/usr\"|g" usr/bin/libgcrypt-config
sed -i "s|/usr/lib/libgpg-error.la|${PWD}/../libgpg-error/usr/lib/libgpg-error.la|g" usr/lib/libgcrypt.la
```

## libssh2

```
git checkout libssh2-1.10.0
sed -i "s|SUBDIRS = src tests docs|SUBDIRS = src|g" Makefile.am
autoscan
autoconf
libtoolize --automake --copy --debug --force
automake --add-missing
CFLAGS="-g -O2 -I${PWD}/../libgpg-error/usr/include -I${PWD}/../libgcrypt/usr/include " LDFLAGS="-L${PWD}/../libgpg-error/usr/lib -L${PWD}/../libgcrypt/usr/lib " ./configure --prefix=/usr --host=aarch64-himix100-linux --disable-shared --disable-rpath --disable-examples-build --with-pic --with-crypto=libgcrypt --with-libgcrypt-prefix=${PWD}/../libgcrypt/usr --without-libz
make
make DESTDIR=${PWD} install
```



#ifndef _WSPP_FILE_SERVER_H_
#define _WSPP_FILE_SERVER_H_

#include <string>
#include <vector>
#include <regex>
#include <iostream>
#include <functional>

#include "zip_fileserver.h"

using RequestQueryParams = std::multimap<std::string, std::string>;

bool parse_query_list(const std::string& strUrl, RequestQueryParams&);

template<class Base>
class Request
{
private:
    const Base* request_;

public:
    explicit Request(const Base* req):request_(req) {};

    const std::string & get_method () const;
    const std::string & get_uri () const;
    const std::string & get_header (const std::string &key) const;
    const std::string & get_body () const;
    const std::string & get_version () const;
};

template<class Base>
class Response
{
private:
    Base* response_;

public:
    explicit Response(Base* rsp):response_(rsp) {};

    void set_status (uint32_t status_);
    void set_body (const std::string &);
    void set_body (const uint8_t* data, uint32_t len);
    void set_header(const std::string& key, const std::string& val);
    void append_header(const std::string& key, const std::string& val);
};

template<class Base>
class WebSocketMsg
{
private:
    const Base* message_;

public:
    explicit WebSocketMsg(const Base* msg):message_(msg) {};
    
    uint32_t get_opcode() const;
    const std::string& get_header ()const;
    const std::string& get_payload ()const;
};

template<class ReqBase, class RspBase, class MsgBase, typename ConnectionType>
class WsFileServer : public ZipFileServer
{
public:
    using WsHandler = std::function<void(ConnectionType& con, const Request<ReqBase>&, const WebSocketMsg<MsgBase>&)>;
    using GetHandler = std::function<bool (ConnectionType& con, const Request<ReqBase>&, Response<RspBase>&)>;
    using ActHandler = std::function<void(ConnectionType& con, const Request<ReqBase>&)>;

private:
    using WsHandlers = std::vector<std::pair<std::regex, WsHandler>>;
    WsHandlers message_handlers_;

    using GetHandlers = std::vector<std::pair<std::regex, GetHandler>>;
    GetHandlers get_handlers_;

    ActHandler  open_handler_;
    ActHandler  close_handler_;
    ActHandler  error_handler_;
    ActHandler  interrupt_handler_;

    static void _on_http(WsFileServer<ReqBase, RspBase, MsgBase, ConnectionType>* svr, ConnectionType hdl){
        if (nullptr != svr) {
            svr->on_http(hdl);
        }
    }
    static void _on_message(WsFileServer<ReqBase, RspBase, MsgBase, ConnectionType>* svr, ConnectionType hdl, MsgBase msg)
    {
        if (nullptr != svr) {
            const WebSocketMsg<MsgBase> msg_(&msg);
            svr->on_message(hdl, msg_);
        }
    }
    static void _on_open(WsFileServer<ReqBase, RspBase, MsgBase, ConnectionType>* svr, ConnectionType hdl){
        if (nullptr != svr) {
            svr->act_handle(hdl, svr->open_handler_);
        }
    }
    static void _on_close(WsFileServer<ReqBase, RspBase, MsgBase, ConnectionType>* svr, ConnectionType hdl){
        if (nullptr != svr) {
            svr->act_handle(hdl, svr->close_handler_);
        }
    }
    static void _on_error(WsFileServer<ReqBase, RspBase, MsgBase, ConnectionType>* svr, ConnectionType hdl){
        if (nullptr != svr) {
            svr->act_handle(hdl, svr->error_handler_);
        }
    }
    static void _on_interrupt(WsFileServer<ReqBase, RspBase, MsgBase, ConnectionType>* svr, ConnectionType hdl){
        if (nullptr != svr) {
            svr->act_handle(hdl, svr->interrupt_handler_);
        }
    }
    void act_handle(ConnectionType& conn_, const ActHandler& handler) {
        try {
            const Request<ReqBase> req_(&get_request(conn_));
            handler(conn_, req_);
        } catch(std::exception &e) {
            std::cout << "exception: " << e.what() << std::endl;
        } catch(...) {
            std::cout << "exception! " << std::endl;
        }
    }

    void on_http(ConnectionType& conn_) {
        try {
            RspBase rsp;
            const Request<ReqBase> req_(&get_request(conn_));

            const std::string& strUrl = req_.get_uri();
            const std::string& strMethod = req_.get_method();
            if (strMethod.compare("GET") == 0) {
                Response<RspBase> rsp_(&rsp);
                if (handle_get_file(strUrl, conn_, req_, rsp_)){
                    send_http_msg(conn_, false, rsp);
                } else {
                    if (dispatch_http_get(strUrl, conn_, req_, rsp_)){
                        send_http_msg(conn_, false, rsp);
                    } else {
                        send_http_msg(conn_, true, rsp);
                    }
                }
            }
        } catch(std::exception &e) {
            std::cout << "exception: " << e.what() << std::endl;
        } catch(...) {
            std::cout << "exception! " << std::endl;
        }
    }
    void on_message(ConnectionType& conn_, const WebSocketMsg<MsgBase>& msg) {
        try {
            const Request<ReqBase> req_(&get_request(conn_));
            const std::string& strUrl = req_.get_uri();
            // dispatch websocket msg
            for (const auto &x : message_handlers_) {
                const auto &pattern = x.first;
                const auto &handler = x.second;
                if (std::regex_match(strUrl, pattern)) {
                    handler(conn_, req_, msg);
                    return;
                }
            }
        } catch(std::exception &e) {
            std::cout << "exception: " << e.what() << std::endl;
        } catch(...) {
            std::cout << "exception! " << std::endl;
        }
    }

protected:
    void*       m_endpoint;

    bool dispatch_http_get(const std::string& path, ConnectionType& con, const Request<ReqBase>& req, Response<RspBase>& rsp) {
        for (const auto &x : get_handlers_) {
            const auto &pattern = x.first;
            const auto &handler = x.second;
            if (std::regex_match(path, pattern)) {
                if (handler(con, req, rsp)) {
                    return true;
                }
            }
        }
        return false;
    };
    bool handle_get_file(const std::string& path, ConnectionType& con, const Request<ReqBase>& req, Response<RspBase>& rsp);

public:
    WsFileServer() : ZipFileServer(), m_endpoint(nullptr) {};

    void* get_endpoint() {return m_endpoint;};

    const ReqBase& get_request(ConnectionType& conn_);

    bool init();
    void run(uint32_t port);

    void send_bin_msg(ConnectionType& con, const uint8_t* data, uint32_t len) const;

    void send_text_msg(ConnectionType& con, const std::string& msg) const;

    void send_http_msg(ConnectionType& con, bool not_found, const RspBase& rsp) const;

    inline WsFileServer& OnOpen(ActHandler handler) {
        open_handler_ = std::move(handler);
        return *this;
    }
    inline WsFileServer& OnClose(ActHandler handler) {
        close_handler_ = std::move(handler);
        return *this;
    }
    inline WsFileServer& OnError(ActHandler handler) {
        error_handler_ = std::move(handler);
        return *this;
    }
    inline WsFileServer& OnInterrupt(ActHandler handler) {
        interrupt_handler_ = std::move(handler);
        return *this;
    }
    inline WsFileServer& Get(const std::string &pattern, GetHandler handler) {
        get_handlers_.push_back(std::make_pair(std::regex(pattern), std::move(handler)));
        return *this;
    }

    inline WsFileServer& WsMsg(const std::string &pattern, WsHandler handler) {
        message_handlers_.push_back(std::make_pair(std::regex(pattern), std::move(handler)));
        return *this;
    }
};

#endif
